import React, { Component } from 'react';

export default class ToDoList extends Component {
    render() {
        if (this.props.items && this.props.items.length > 0) {
            return (
                <ul>
                    {this.props.items.map((item, index) => <li key={index}>{item}</li>)}
                </ul>
            )                  
        }
        else {
            return "There are no to-do items!";
        }
    }
}